import $, { get } from "jquery";
import * as d3 from "d3";

import { w3cwebsocket as W3CWebSocket } from "websocket";

// Spinning the http server and the websocket server.

const url = 'ws://127.0.0.1:8000';
const client = new W3CWebSocket(url);

const subscriptionListSel = $("#subscriptionList");
const subscribeInputSel = $('#subscribeInput');

const subscribedList = [];
const plots = {};

class RollingArray {
    constructor(n, d0) {
        this.arr = Array(n).fill(d0);
    }
    addValue(t, x) {
        this.arr.shift();
        this.arr.push([t, x]);
    }
    array() { return this.arr; }
    extract(i) { return this.arr.map(d => d[i]); }
}

function drawPlot(plot) {
    const { plotSel, data, dataSel, xAxisSel, yAxisSel, width, height } = plot;
    console.log(plot);

    // x Axis
    const x = d3.scaleTime()
        .domain(d3.extent(data.extract(0)))
        //d3.scaleLinear()
        //.domain(data.domain(0))
        .range([0, width]);
    xAxisSel.call(d3.axisBottom(x));

    // y Axis
    const y = d3.scaleLinear()
        .domain(d3.extent(data.extract(1)))
        .range([height, 0]);
    yAxisSel.call(d3.axisLeft(y));

    // Plot the area
    dataSel.datum(data.array())
        .attr("d", d3.line()
            .curve(d3.curveBasis)
            .x(function (d) { return x(d[0]); })
            .y(function (d) { return y(d[1]); })
        );
}

function addPlot(channelID) {
    const plotData = new RollingArray(30, [new Date(), 0]);


    // set the dimensions and margins of the graph
    var margin = { top: 30, right: 30, bottom: 30, left: 50 },
        width = 600 - margin.left - margin.right,
        height = 200 - margin.top - margin.bottom;

    // append the svg object to the body of the page
    var plotSel = d3.select("#plots")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

    const xAxisSel = plotSel.append("g")
        .attr("transform", "translate(0," + height + ")");
    const yAxisSel = plotSel.append("g")
    const dataSel = plotSel.append("path")
        .attr("class", "mypath")
        //.attr("fill", "#69b3a2")
        .attr("fill", "none")
        //.attr("opacity", ".8")
        .attr("stroke", "#000")
        .attr("stroke-width", 1);
    //.attr("stroke-linejoin", "round");

    plots[channelID] = { data: plotData, plotSel: plotSel, dataSel: dataSel, xAxisSel: xAxisSel, yAxisSel: yAxisSel, width: width, height: height };

    drawPlot(plots[channelID]);
}



subscribeInputSel.on("keypress", function (e) {
    if (e.which == 13) {
        const channelID = $(subscribeInput).val();
        subscribedList.push(channelID);
        addPlot(channelID);

        // send request      
        const request = {
            type: "SUBSCRIBE",
            data: {
                channelID: channelID
            }
        }
        client.send(JSON.stringify(request));

        console.log("SUBSCRIBE: ", request);

        // reset input text
        $(subscribeInputSel).val("");
        // update summary
        $("#subscribedList").text(subscribedList.join(', '))
    }

})

client.onopen = () => {
    $("#serverURL").text(url);
    console.log(`CONNECTED to ${url}`);
};
client.onmessage = (message) => {
    const { type, data } = JSON.parse(message.data);
    if (type == "HELLO") {
        console.log(`HELLO ${JSON.stringify(data)}`);
        data.map(el => subscriptionListSel.append($(`<li>${el}</li>`)))
    }
    else if (type == "PUBLISH") {
        const { channelID, content } = data;
        console.log(`PUBLISH ${JSON.stringify(data)}`);

        const time = new Date();
        plots[channelID].data.addValue(time, content);
        drawPlot(plots[channelID]);
    }
};
console.log('INIT done');




/*
// get the data
d3.csv("https://raw.githubusercontent.com/holtzy/data_to_viz/master/Example_dataset/1_OneNum.csv").then(function (data) {
    console.log("n data: ", data.length);
    // add the x Axis


    // Compute kernel density estimation
    var kde = kernelDensityEstimator(kernelEpanechnikov(7), x.ticks(40))
    var density = kde(data.map(function (d) { return d.price; }))

    // Plot the area
    svg.append("path")
        .attr("class", "mypath")
        .datum(density)
        .attr("fill", "#69b3a2")
        .attr("opacity", ".8")
        .attr("stroke", "#000")
        .attr("stroke-width", 1)
        .attr("stroke-linejoin", "round")
        .attr("d", d3.line()
            .curve(d3.curveBasis)
            .x(function (d) { return x(d[0]); })
            .y(function (d) { return y(d[1]); })
        );

}).catch(function (error, rows) {
    console.log("error:", error, rows);
});


// Function to compute density
function kernelDensityEstimator(kernel, X) {
    return function (V) {
        return X.map(function (x) {
            return [x, d3.mean(V, function (v) { return kernel(x - v); })];
        });
    };
}
function kernelEpanechnikov(k) {
    return function (v) {
        return Math.abs(v /= k) <= 1 ? 0.75 * (1 - v * v) / k : 0;
    };
}*/