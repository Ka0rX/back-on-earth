from vnpy import *
import numpy as np
import time
import matplotlib.pyplot as plt

def to_mat3f(M):
    return mat3f(M[0,0], M[0,1], M[0,2], M[1,0], M[1,1], M[1,2], M[2,0], M[2,1], M[2,2])

def from_mat3f(M):
    return np.array([[M.e00,M.e01,M.e02],[M.e10,M.e11,M.e12],[M.e20,M.e21,M.e22]])

def from_vec3f(M):
    return np.array([M.x, M.y, M.z])

def to_rad(x):
    return x*np.pi/180

def rotate(phi, theta, psi):
    """
    x, y, z = phi, theta, psi = roll, pitch, yaw 
    """
    M = np.zeros((3,3))
    M[0] = [np.cos(psi) * np.cos(theta), np.sin(psi)*np.cos(theta), -np.sin(theta) ]
    M[1] = [-np.sin(psi) * np.cos(phi), np.cos(phi)*np.cos(psi),   np.sin(theta)]
    M[2,0] = np.sin(psi) * np.sin(phi) + np.cos(phi)*np.sin(theta)*np.cos(psi)
    M[2,1] = -np.cos(psi) * np.sin(phi)+np.sin(psi)*np.sin(theta)*np.cos(phi)
    M[2,2] = np.cos(phi) * np.cos(theta)

    return np.linalg.inv(M)

def read_ypr_accel():
    reg = s.read_yaw_pitch_roll_magnetic_acceleration_and_angular_rates()
    return to_rad(from_vec3f(reg.yaw_pitch_roll)), from_vec3f(reg.accel)

def integrate(dx, dt):
    n = len(dx)
    x = (n) * [dx[0]]
    for i in range(1, n):
        x[i] = x[i-1] + dt * dx[i-1]
    return x

def deriv(x, dt):
    n = len(x)
    dx = (n) * [dx[0]]
    for i in range(n):
        dx[i] = x[i-1] + dt * dx[i-1]
    return x


freq = 20
dt = 1/freq

s = VnSensor()
s.connect('/dev/ttyUSB0', 115200)
s.write_async_data_output_frequency(freq)

print(s.read_model_number())

# create websocket client
import json
import pprint
import time
import websocket
from websocket import create_connection

#websocket.enableTrace(True)
ws = create_connection('ws://localhost:8000')

result = ws.recv()
print('Result: {}'.format(result))


# utilities
def sendToServer(data, t):
    ws.send(json.dumps({"data":data,"type":t}))

def getChannelID(channel):
    return f'channel-{channel}'

def registerChannel(channelID):
    data = {"channelID":channelID}
    sendToServer(data, "REGISTER")
    
def publish(channelID, content):
    data = {"channelID":channelID, "content":content}
    sendToServer(data, "PUBLISH")

# register channels
name = ["yaw","pitch","roll", "ax", "ay", "az"]
for i in range(6):
    registerChannel(getChannelID(name[i]))


# publish data
while True:
    vals = np.array(read_ypr_accel()).flatten()
    
    for i,x in enumerate(vals):
        publish(getChannelID(name[i]), x)

    time.sleep(dt)