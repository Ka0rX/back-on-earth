import pigpio
import time
import math
import numpy as np

pi = pigpio.pi()

servos = [(5, 2250), (6, 1200), (13, 900), (19, 1050)]

def angleToPulse(id, angle):
    ispos = (id < 2)
    dir = 2*ispos-1
    return servos[id][1] + int(40*angle*dir)

def actionner(ids, angle):
    for id in ids:
        pi.set_servo_pulsewidth(servos[id][0], angleToPulse(id, angle))

def test_sin(T, fe, f = 1):
    for t in np.linspace(0, T, T*fe):
        x = 3*math.sin(2*math.pi*f*t)
        print(f"x={x}")
        actionner([0, 1, 2, 3], x)
        time.sleep(1/fe)

def calibrateServo(id, pin, pulse):
    print(f"[>>] Servo {id} pin={pin}")
    print(f"**************************")
    if input(f"[>] Try saved pulse={pulse}? ") == '':
        pi.set_servo_pulsewidth(pin, pulse)
    while True:
        command = input(f"[>] Enter new pulse: ")
        try:
            pulse = int(command)
            pi.set_servo_pulsewidth(pin, pulse)
        except ValueError:
            break
    print(f"[<] Done calibrating servo {id} pin={pin}")
    print(f"[<] Saved pulse={pulse}")
    print()

print(f"[>] Starting calibration")
print()

for k,(pin, pulse) in enumerate(servos):
    calibrateServo(k, pin, pulse)

print(f"[<] Done calibration")

print(f"[>] Summary")
print(f"servo\tpin\tpulse")
for k,(pin, pulse) in enumerate(servos):
    print(f"{k}\t{pin}\t{pulse}")

pi.stop()