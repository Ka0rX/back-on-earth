import pigpio, time
pi = pigpio.pi()
pi.set_PWM_frequency(12, 50)
pi.set_PWM_frequency(13, 50)

pi.set_servo_pulsewidth(12, 1000)
pi.set_servo_pulsewidth(13, 1000)


def servo(pin, pw, t = 2):
    pi.set_servo_pulsewidth(pin, pw)
    try:
        time.sleep(t)
    finally:
        pi.set_servo_pulsewidth(pin, 1000)
        print("powered off")

