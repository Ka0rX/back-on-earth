// https://gist.github.com/tuxmartin/1218851b7e025f68ecc50f949c9dd332
#include "Poco/Net/HTTPRequest.h"
#include "Poco/Net/HTTPResponse.h"
#include "Poco/Net/HTTPMessage.h"
#include "Poco/Net/WebSocket.h"
#include "Poco/Net/HTTPClientSession.h"
#include "Poco/JSON/JSON.h"
#include "Poco/JSON/Object.h"
#include "Poco/JSON/Parser.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <thread>
#include <chrono>

using namespace std;
using Poco::Net::HTTPClientSession;
using Poco::Net::HTTPRequest;
using Poco::Net::HTTPResponse;
using Poco::Net::HTTPMessage;
using Poco::Net::WebSocket;
using Poco::JSON::Object;
using Poco::JSON::Parser;

WebSocket* m_psock;
string userName;

void sendServer(std::string buf) {
    int len=m_psock->sendFrame(buf.c_str(),buf.size(),WebSocket::FRAME_TEXT);
    std::cout << "Sent bytes " << len << std::endl;
}

void sendCommand(string type, const Object::Ptr &data) {
    Object json;
    json.set("type", type);
    json.set("data", data);

    stringstream ss;
    json.stringify(ss);
    json.stringify(std::cout);
    sendServer(ss.str());
}

string recvServer() {
    string buf;
    buf.resize(256);
    int flags=0;
    int nrec = m_psock->receiveFrame(&buf[0],256,flags);
    buf.resize(nrec);
    return buf;
}

string channelName(int i) {
    return userName + '-' + to_string(i);
}

void publishToChannel(int i, float content) {
    Object::Ptr data = new Object();
    data->set("channelID", channelName(i));
    data->set("content", content);
    sendCommand("PUBLISH", data);
}

void registerChannel(int i) {
    Object::Ptr data = new Object();
    data->set("channelID", channelName(i));
    sendCommand("REGISTER", data);
}

float randf() {
    return static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
}

int main(int args,char **argv)
{
    srand(time(0));
    string url = "localhost";
    int port = 8000;
    userName = "RPI-"+to_string(rand() % 100);

    HTTPClientSession cs(url, port);    
    HTTPRequest request(HTTPRequest::HTTP_GET, "/?encoding=text",HTTPMessage::HTTP_1_1);
    request.set("origin", userName);
    HTTPResponse response;

    try {

        m_psock = new WebSocket(cs, request, response);

        // HELLO

        std::cout << "RECEIVED: " << recvServer() << std::endl;


        // REGISTER
        for(int i = 0; i < 4; i++) 
            registerChannel(i);

        while(true) {
            //Object::Ptr content = new Object();
            //content->set("x", randf());
            //content->set("y", randf());
            for(int i = 0; i < 4; i++) 
                publishToChannel(i, randf());
            //content->stringify(std::cout);
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        }

        m_psock->close();

    } catch (std::exception &e) {
        std::cout << "Exception " << e.what();
    }

}