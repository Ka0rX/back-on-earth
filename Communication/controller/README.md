## Tree 
The files are organized as follows: 

* In folder `vendor` we have all the libraries and drivers to communicate with
  all the devices. **Please for all the libraries that are available on git
  repository, add them as submodules**

* In folder `src` there's the file `sketch.h` that contains the `setup` and
  `loop` function as in the Arduino. Add your code there.

## Build the code

1. Create a `build` directory 
```
mkdir build
```
2. Enter the directory and run `cmake`
```
cmake .. 
```
3. Build the code 
```
make -j 4
```

4. Install the code
```
make install
``` 



You will find the binary in `build/run/bin/` (`boe` stands for
**B**ack**O**n**E**arth)
