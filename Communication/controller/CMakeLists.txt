cmake_minimum_required(VERSION 3.5)
cmake_policy(SET CMP0048 NEW)

# Local functions and macros
list(APPEND CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake)

project(BackOnEarth VERSION 0.1.0 LANGUAGES CXX)

#######################################################################
#                       Install Prefix Settings                       #
#######################################################################
include(GNUInstallDirs)

set(CMAKE_INSTALL_PREFIX "${CMAKE_BINARY_DIR}/run" 
    CACHE PATH
    "Install path prefix, prepended onto install directories."
    FORCE
)

#######################################################################
#                       Build Vendor Libraries                        #
#######################################################################
add_subdirectory(vendor)
message(STATUS "Vendor libraries: ${VENDOR_LIBRARIES}")


#######################################################################
#                         Build Library Code                          #
#######################################################################
add_subdirectory(src)
