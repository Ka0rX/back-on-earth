#include <iostream>
#include <string>
#include <linux/types.h>

// Lidar 
#include "lidarlite_v3.h"
LIDARLite_v3 lidar;
bool busyFlag;
int distance;

// IMU
#include "vn/ezasyncdata.h"

using namespace vn::sensors;

const std::string imuPort = "/dev/ttyUSB0";
const int imuBaudRate = 115200;
EzAsyncData* imu;


void setup() {
    // Initialize the lidar i2c
    lidar.i2c_init();

    // Configure the mode of the lidar. Check the manual
    lidar.configure(1);

    // Initialize the imu
    imu = EzAsyncData::connect(imuPort, imuBaudRate);

}

void loop() {
    // Block until next read is available
    lidar.waitForBusy();
    
    // When no longer busy, immediately initialize another measurement
    // and then read the distance data from the last measurement.`
    // This method will result in faster I2C rep rates.
    lidar.takeRange();
    distance = lidar.readDistance();
    
    // std::cout << "Lidar Distance: " << distance << std::endl;
    
    // Read Yaw, pitch, roll data (block until next read is available
    CompositeData cd = imu->getNextData();
    
    //std::cout << "Current YPR: " << cd.yawPitchRoll() << std::endl;
    std::string ypr = vn::math::str(cd.yawPitchRoll());
    printf("\rDistance: %dcm    \tYPR: %s    ", distance, ypr.c_str());
}
