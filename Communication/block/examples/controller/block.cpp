#include "block.h"

int main()
{
    BlockDiagram bd;
    bd.readFromFile("./block.1");

    double dt = 1e-3;
    for(int i = 0, N = 10; i < N; i++)
        cout << bd.run(dt) << '\n';
    cout << endl;

    return 0;
}