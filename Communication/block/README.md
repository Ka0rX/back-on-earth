## Build the code

1. Create a `build` directory 
```
mkdir build
```
2. Enter the directory and run `cmake`
```
cmake .. 
```
3. Build the code 
```
make -j 4
```

4. Install the code
```
make install
``` 

You will find the binary in `build/run/bin/`
