#include <iostream>
#include <vector>
#include <cassert>
#include <fstream>

using namespace std;

using ftype = double;

class Block {
public:
    virtual ftype calc(ftype dt) = 0;
    virtual ~Block() {}

    void link(Block *b) {
        m_args.push_back(b);
    }

    int id;

protected:
    vector<Block*> m_args;
};

typedef ftype (*input_fn)();

struct InputBlock : public Block {
    InputBlock(input_fn c) : m_input(c) {}
    virtual ~InputBlock() {}

    virtual ftype calc(ftype dt) {
        return (*m_input)();
    }

protected:
    input_fn m_input;
};

struct ConstBlock : public Block {
    ConstBlock(ftype c) : m_const(c) {}
    virtual ~ConstBlock() {}

    virtual ftype calc(ftype dt) {
        return m_const;
    }

protected:
    ftype m_const;

};

struct SumBlock : public Block {
    SumBlock(Block *a, Block *b) : m_a(a), m_b(b) {}
    virtual ~SumBlock() {}

    virtual ftype calc(ftype dt) {
        assert(m_a != nullptr && m_b != nullptr);
        return m_a->calc(dt) + m_b->calc(dt);
    }
protected:
    Block *m_a, *m_b;
};

struct DiffBlock : public Block {
    DiffBlock(Block *a, Block *b) : m_a(a), m_b(b) {}
    virtual ~DiffBlock() {}

    virtual ftype calc(ftype dt) {
        assert(m_a != nullptr && m_b != nullptr);
        return m_a->calc(dt) - m_b->calc(dt);
    }
protected:
    Block *m_a, *m_b;
};

struct ProdBlock : public Block {
    ProdBlock(Block *a, Block *b) : m_a(a), m_b(b) {}
    virtual ~ProdBlock() {}

    virtual ftype calc(double dt) {
        assert(m_a != nullptr && m_b != nullptr);
        return m_a->calc(dt) * m_b->calc(dt);
    }
protected:
    Block *m_a, *m_b;
};

struct ConstProdBlock : public Block {
    ConstProdBlock(Block *a, ftype c) : m_const(c), m_a(a) {}
    virtual ~ConstProdBlock() {}

    virtual ftype calc(ftype dt) {
        assert(m_a != nullptr);
        return m_a->calc(dt) * m_const;
    }
protected:
    Block *m_a;
    ftype m_const;
};

struct ForwardIntBlock : public Block {
    ForwardIntBlock(Block *a, ftype k, ftype ic) : m_a(a), m_gain(k), m_int(ic) {}
    virtual ~ForwardIntBlock() {}

    virtual ftype calc(ftype dt) {
        assert(m_a != nullptr);
        m_int += m_gain * dt * m_a->calc(dt);
        return m_int;
    }
protected:
    Block *m_a;
    ftype m_gain, m_int;
};

struct DerivBlock : public Block {
    DerivBlock(Block *a, ftype k) : m_a(a), m_gain(k), m_prev(0.0) {}
    virtual ~DerivBlock() {}

    virtual ftype calc(ftype dt) {
        assert(m_a != nullptr);
        ftype cur = m_a->calc(dt);
        ftype der = m_gain * (cur-m_prev) / dt;
        m_prev = cur;
        return der;
    }
protected:
    Block *m_a;
    ftype m_prev, m_gain;
};


// par convention, main = bloc derniere ligne

struct BlockDiagram {
    vector<Block*> blocks;
    Block *main;

    ftype run(ftype dt) {
        assert(main != nullptr);
        return main->calc(dt);
    }

    ~BlockDiagram() {
        for(Block *b : blocks) {
            delete b;
            b = nullptr;
        }
        blocks.clear();
    }

    void readFromFile(const char* filename) {
        ifstream file(filename);
        if(!file.is_open())
            throw runtime_error("Fichier introuvable");

        for(string line; getline(file, line); )
            cout << line << endl, addBlock(line);
        main = blocks.back();
        assert(main != nullptr);
    }

    void addBlock(string const &s) {
        char type = s[0];
        Block *b = nullptr;
        int a1, a2;
        double f1, f2;

        switch(type) {
            case '+':
                sscanf(s.c_str(), "+ %d%d", &a1, &a2);
                //cout << "++ " << a1 << " " << a2 << " " << blocks.size() << endl;
                assert(a1 >= 0 && a1 < blocks.size());
                assert(a2 >= 0 && a2 < blocks.size());
                b = new SumBlock(blocks[a1], blocks[a2]);
                break;
            case '-':
                sscanf(s.c_str(), "- %d%d", &a1, &a2);
                assert(a1 >= 0 && a1 < blocks.size());
                assert(a2 >= 0 && a2 < blocks.size());
                b = new DiffBlock(blocks[a1], blocks[a2]);
                break;
            case '*':
                sscanf(s.c_str(), "* %d%d", &a1, &a2);
                assert(a1 >= 0 && a1 < blocks.size());
                assert(a2 >= 0 && a2 < blocks.size());
                b = new ProdBlock(blocks[a1], blocks[a2]);
                break;
            case 'i':
                sscanf(s.c_str(), "i %d%lf%lf", &a1, &f1, &f2);
                assert(a1 >= 0 && a1 < blocks.size());
                b = new ForwardIntBlock(blocks[a1], f1, f2);
                break;
            case 'd':
                sscanf(s.c_str(), "d %d%lf", &a1, &f1);
                assert(a1 >= 0 && a1 < blocks.size());
                b = new DerivBlock(blocks[a1], f1);
                break;
            // TODO: ajouter constprod ???
            case 'c':
                sscanf(s.c_str(), "c %lf", &f1);
                //cout << "cc " << f1 << endl;
                b = new ConstBlock(f1);
                break;
            default: throw runtime_error("Type de bloc inconnu");
        }
        assert(b != nullptr);
        blocks.push_back(b);
    }
};
