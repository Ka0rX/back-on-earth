// https://blog.logrocket.com/websockets-tutorial-how-to-go-real-time-with-node-and-react-8e4693fbf843/

const webSocketsServerPort = 8000;
const webSocketServer = require('websocket').server;
const http = require('http');
// Spinning the http server and the websocket server.
const server = http.createServer();
server.listen(webSocketsServerPort);
const wsServer = new webSocketServer({
    httpServer: server
});

const connections = [];
const clients = [];
const channels = {};

class Channel {
    constructor(name) {
        this.clients = {};
        this.name = name;
        console.log("Applied constructor on " + name);
    }

    subscribe(id) {
        this.clients[id] = { id: id };
        this.sendTo(id, { type: "log", data: `Subscribed to channel ${this.name}` });
    }

    unsubscribe(id) {
        delete this.clients[id];
    }

    broadcast(msg) {

        Object.keys(this.clients).map((client) => {
            this.sendTo(client, msg);
        });
    }

    sendTo(id, msg) {
        connections[id].send(JSON.stringify(msg));
    }
}

const typesDef = {
    SUBSCRIBE: "SUBSCRIBE",
    REGISTER: "REGISTER",
    PUBLISH: "PUBLISH",
}

wsServer.on('request', function (request) {
    // You can rewrite this part of the code to accept only the requests from allowed origin
    const connection = request.accept(null, request.origin);
    const userID = connections.length;

    connections.push(connection);
    clients[userID] = { id: userID, channels: [] };

    console.log(`    ${new Date()} - CONNECTED user ${userID} @ ${request.origin}`);

    connection.sendUTF(JSON.stringify({ type: "HELLO", data: Object.values(channels).map(channel => channel.name) }));


    connection.on('message', function (message) {
        if (message.type !== "utf8")
            return;

        const { type, data } = JSON.parse(message.utf8Data);
        //console.log(`   RECEIVED message type=${type} data=${JSON.stringify(data)}`);

        if (type === typesDef.REGISTER) {
            const { channelID } = data;
            clients[userID].channels.push(channelID);
            channels[channelID] = new Channel(channelID);
            console.log(`    REGISTERED channel id=${channelID}`);
        }

        else if (type === typesDef.SUBSCRIBE) {
            const { channelID } = data;
            if (!channels.hasOwnProperty(channelID))
                return;
            channels[channelID].subscribe(userID);
            console.log(`    SUBSCRIBED user id=${userID} to channel id=${channelID}`);
        }

        else if (type === typesDef.PUBLISH) {
            const { channelID, content } = data;
            channels[channelID].broadcast({ type: "PUBLISH", data: data });
            //console.log(`        PUBLISHED channel=${channelID}: ${JSON.stringify(content)}`);
        }
        else {
            console.log(`[X] UNRECOGNIZED message type=${type} data=${JSON.stringify(data)}`);
        }
    });
    // user disconnected
    connection.on('close', function () {
        console.log(`    ${new Date()} - DISCONNECTED user id=${userID}`);
        delete connections[userID];
        Object.values(channels).forEach(channel => channel.unsubscribe(userID));
    });
});

/*function loop() {

  setTimeout(loop, 100);
}

loop();*/