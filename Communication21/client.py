import asyncio
import websockets
import time

ip_addr = "172.20.10.6"


async def test():


    async with websockets.connect('ws://' + ip_addr + ':8000') as websocket:
        await websocket.send("hello")


asyncio.get_event_loop().run_until_complete(test())
