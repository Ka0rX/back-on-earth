# https://www.piesocket.com/blog/python-websocket

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from threading import Thread
import flask
from flask import request
# We want to create a server that plots the information in real-time/

X = [0]
Y = [0]
Z = [0]

fig = plt.figure()
ax = plt.axes(projection='3d')

ax.set_xlim3d([0.0, 10.0])
ax.set_xlabel('X')

ax.set_ylim3d([0.0, 10.0])
ax.set_ylabel('Y')

ax.set_zlim3d([0.0, 8.0])
ax.set_zlabel('Z')

ax.set_title('3D Test')




app=flask.Flask("TEST")
@app.route("/",methods=['GET'])
def route() :
    X.append(float(request.args.get('X')))
    Y.append(float(request.args.get('Y')))
    Z.append(float(request.args.get('Z')))
    return "Done"


def run_app() :
    app.run(host="0.0.0.0")

plt.ion()

thread = Thread(target=run_app)
thread.daemon = False
thread.start()

len_X = 0

while True:

    if (len_X < len(X)):
        len_X = len(X)
        plt.plot(X, Y, Z,'k')
    plt.pause(0.1)


plt.show()


