import math
import numpy as np
import scipy.linalg as linalg
from Controle.KalmanFilter import KalmanFilter


class Control:
    # finalPos: xfinal,yfinal,zfinal
    def __init__(self, dt, kalmanFilterOn, kalmanFilterAltitudeOn):
        self.kalmanFilterOn = kalmanFilterOn
        self.kalmanFilterAltitudeOn = kalmanFilterAltitudeOn
        self.dt = dt
        # For the LQR
        self.M = 2.117
        self.g = 9.81
        self.P = self.M * self.g
        self.Fe = self.M * self.g
        # a -> coeficient de force pour les ailettes
        # TODO: Changer
        self.a = 0.1 * 0.25
        self.L = 0.3
        self.J = 0.4
        # Rayon a priori
        # TODO: Recalculer
        self.d = 0.05

        # Bryson's Rule
        # Qi = 1/max(xi)²
        # Ri = 1/max(ui)²

        self.Q = np.array([[100, 0, 0, 0, 0],
                           [0, 100, 0, 0, 0],
                           [0, 0, 100, 0, 0],  # de l'ordre de 3° pour le tangage et le lacet
                           [0, 0, 0, 100, 0],
                           [0, 0, 0, 0, 100]])
        self.R = np.array([[10, 0, 0],  # de l'ordre de 20° pour les ailettes
                           [0, 10, 0],
                           [0, 0, 10]])

        # Instanciate the Kalman Filter Object
        self.kalmanFilter = KalmanFilter(dt, self.M, self.P)
        self.B = dt * np.array([[0, 0, 0],
                                [self.a * self.d / self.J, self.a * self.d / self.J, self.a * self.d / self.J],
                                [0, 0, 0],
                                [self.L / self.J, -self.L / (2 * self.J), -self.L / (2 * self.J)],
                                [0, math.sqrt(3) * self.L / (2 * self.J), -math.sqrt(3) * self.L / (2 * self.J)]])
        self.A = np.eye(5) + self.dt * np.array([[0, 1, 0, 0, 0],
                                                 [0, 0, 0, 0, 0],
                                                 [0, 0, 0, 1, 0],
                                                 [0, 0, 0, 0, 0],
                                                 [0, 0, 0, 0, 0]])

    #### Step
    # xk is the estimation

    # In the basis where x' is vertical there is x' =z, y'=x, z'= y with x,y,z the classic base
    def step(self, estimate, cov, Pk, measure, U):

        # Kalman Filter on measures
        if (self.kalmanFilterOn):
            estimate, cov = self.kalmanFilter.kalmanFilterUpdate(self.A, self.B, estimate, U, cov, measure, self.Q_Kf)
        else:
            estimate = measure

        # LQR for horizontal and angular control
        P = linalg.solve_discrete_are(self.A, self.B, self.Q, self.R, e=None, s=None, balanced=True)
        F_lqr = np.linalg.pinv(self.R + self.B.T @ P @ self.B) @ self.B.T @ P @ self.A  # F deja pris...
        u = -F_lqr @ estimate

        return u, estimate, cov, Pk
