import numpy as np
from math import sqrt
import scipy.linalg as linalg
import matplotlib.pyplot as plt
from simple_pid import PID
import time

# Supress scientific notation when printing NumPy arrays
# np.set_printoptions(precision=5,suppress=True)
#Yani=bg
# Optional Variables
max_fin_angle = 45  # degree
noisePositionLevel = 0.1
noiseAngleLevel = 0.001
noiseAltitudeLevel = 0.01
noiseAltitudeVelocityLevel = 0.01
M = 4
g = 9.81
Poids = M * g
Fe = M * g
a = 0.1 * 0.25
L = 0.3
J = 0.4
d = 0.05
dt = 0.1
Q_Kf = 0.5e-5 * np.eye(10)
Q_altitude = 4.7e-6 * np.eye(2)
A_altitude = np.eye(2)
A_altitude[0, 1] = dt
B_altitude = np.array([0, dt])


def getA(F):
    dA = np.array([[0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, F / M, 0],
                   [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, -F / M, 0, 0, 0],
                   [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                   [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
    A = np.eye(10) + dt * dA
    return A


A = getA(Fe)

B = dt * np.array([[0, 0, 0],
                   [0, sqrt(3) * a / (2 * M), -sqrt(3) * a / (2 * M)],
                   [0, 0, 0],
                   [-a / M, a / (2 * M), a / (2 * M)],
                   [0, 0, 0],
                   [a * d / J, a * d / J, a * d / J],
                   [0, 0, 0],
                   [L / J, -L / (2 * J), -L / (2 * J)],
                   [0, 0, 0],
                   [0, sqrt(3) * L / (2 * J), -sqrt(3) * L / (2 * J)]])

R = 1000 * np.identity(3)  # Penalty for angular velocity effort
Q = 0.1 * np.identity(10)
Q[4, 4] = 1000
Q[6, 6] = 1000
Q[8, 8] = 1000


def kalmanFilterUpdate(A, B, X, u, P, z, q):
    xf, Pf = kalmanFilterPredict(A, B, X, u, P, q)

    # Computation of the Observation Matrix
    H = np.eye(10)

    # Computation of Measurement Uncertainty
    R = np.eye(10)
    for i in range(4):
        R[i, i] = pow(noisePositionLevel, 2)
    for i in range(4, 10):
        R[i, i] = pow(noiseAngleLevel, 2)

    # Computation of the Kalman gain
    Ht = H.transpose()
    K = Pf @ Ht @ np.linalg.inv((H @ Pf @ Ht + R))

    # State update
    x = xf + K @ (z - H @ xf)

    # Covariance update
    IKH = (np.eye(10) - K @ H)
    IKHt = IKH.transpose()
    Pnew = IKH @ Pf @ IKHt @ +K @ R @ (K.transpose())

    return x, Pnew


def kalmanFilterPredict(A, B, X, u, P, q):
    xf = (A @ X) + (B @ u)

    Pf = A @ P @ (A.transpose()) + q

    return xf, Pf


def kalmanFilterAltitude(vecAltitude, Fe, P, vec_measure, q):
    vecAltitudef, Pf = kalmanFilterPredictAltitude(vecAltitude, Fe, P, q)

    # Computation of the Observation Matrix
    H = np.eye(2)

    # Computation of Measurement Uncertainty
    R = np.eye(2)
    R[0, 0] = pow(noiseAltitudeLevel, 2)
    R[1, 1] = pow(noiseAltitudeVelocityLevel, 2)

    # Computation of the Kalman gain
    Ht = H.transpose()
    K = Pf @ Ht @ np.linalg.inv((H @ Pf @ Ht + R))

    # State update
    vecAltitude = vecAltitudef + K @ (vec_measure - H @ vecAltitudef)

    # Covariance update
    IKH = (np.eye(2) - K @ H)
    IKHt = IKH.transpose()
    Pnew = IKH @ Pf @ IKHt @ +K @ R @ (K.transpose()) + q

    return vecAltitude, Pnew


def kalmanFilterPredictAltitude(vecAltitude, Fe, P, q):
    vecAltitudef = A_altitude @ vecAltitude + B_altitude * (Fe - Poids) / M
    P = A_altitude @ P @ (A_altitude.transpose()) + q
    return vecAltitudef, P


def main():
    T = [0]
    altitude = 0
    v = 0
    x = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    x_final = np.array([3, 0, 3, 0, 0, 0, 0, 0, 0, 0])
    cov = 0.0001 * np.identity(10)
    xk = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    z = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
    X = [x[5]]
    Z = [x[0]]
    Xk = [xk[0]]
    E = 0
    pid = PID(0.07, 0.0001, 1, setpoint=3)
    # pid.output_limits = (0, 2*Poids)
    pid.sample_time = 0
    max_temps_pas = 0
    for i in range(1000):
        t_actuel = time.time()
        print(f't = {dt * i}s')
        T.append(T[-1] + dt)

        Fe = pid(altitude, 0.1) + Poids
        A = getA(Fe)
        print("Fe = " + str(Fe))
        print("Altitude = " + str(altitude))
        v += dt * (Fe - Poids) / M
        altitude += dt * v

        # cf wikipedia https://en.wikipedia.org/wiki/Linear%E2%80%93quadratic_regulator
        # cas discret horizon infini
        P = linalg.solve_discrete_are(A, B, Q, R, e=None, s=None, balanced=True)
        F_lqr = np.linalg.pinv(R + B.T @ P @ B) @ B.T @ P @ A  # F deja pris...
        u = -F_lqr @ (xk - x_final)
        # u = -F_lqr @ (z - x_final)
        x = (A @ x) + (B @ u)
        X.append(x[4])
        noisePosition = np.random.normal(0, noisePositionLevel, 4)
        noiseAngle = np.random.normal(0, noiseAngleLevel, 6)
        noise = np.concatenate([noisePosition, noiseAngle])
        z = x + noise
        Z.append(z[4])
        xk, cov = kalmanFilterUpdate(A, B, xk, u, cov, z, Q_Kf)
        Xk.append(xk[4])
        e = np.linalg.norm(x - xk)
        E += e * e

        print(f'u = {u}')
        print(f'x = {x}')
        print(f'x_final = {x_final}')
        error = np.linalg.norm(xk - x_final)
        print(f'error = {error:.4f}')
        print("temps d'une étape :" + str(time.time() - t_actuel))

        # Stop as soon as we reach the goal
        # Feel free to change this threshold value.
        if error < 0.1 or i == 999:
            print("\nGoal Has Been Reached Successfully!")
            print("E=" + str(E))
            plt.figure()
            plt.plot(T, X, label='true y')
            # plt.plot(T,Z, label='measure')
            # plt.plot(T,Xk, label='after kalman filter')
            plt.xlabel("Temps (s)")
            plt.ylabel("Angle ailette 1 (rad)")
            # plt.legend()
            plt.show()
            break


# Entry point for the program
# main()

def testPID(a=Q_altitude):
    q = a * np.eye(2)
    pid = PID(0.07, 0.0001, 1, setpoint=3)
    # sans dépasseemnt :   pid = PID(0.07, 0.0001,1, setpoint=3)
    # rapide :     pid = PID(0.2, 0.0001,1.25, setpoint=3)
    pid.output_limits = (-Poids, Poids)
    pid.sample_time = 0
    T = [0]
    vec_reel = np.array([0, 0])
    altitude = 0
    vec_altitude = np.array([0, 0])
    v = 0
    Pk = 0.0001 * np.identity(2)
    tab = [0]
    mesure = [0]
    aKF = [0]
    Fmax = 0
    E = 0

    for i in range(1000):
        T.append(T[-1] + dt)
        Fe = pid(vec_reel[0], 0.1) + Poids
        if abs(Fe - Poids) > Fmax:
            Fmax = abs(Fe - Poids)
        # v+=dt*(Fe-Poids)/M
        # altitude+=dt*v
        vec_reel = A_altitude @ vec_reel + B_altitude * (Fe - Poids) / M
        noisePosition = np.random.normal(0, noiseAltitudeLevel)
        noiseVelocity = np.random.normal(0, noiseAltitudeVelocityLevel)
        v_mesure = vec_reel[1] + noiseVelocity
        altitude_mesure = vec_reel[0] + noisePosition
        vec_measure = np.array([v_mesure, altitude_mesure])
        vec_altitude, Pk = kalmanFilterAltitude(vec_altitude, Fe, Pk, vec_measure, q)
        tab.append(vec_reel[0])
        mesure.append(altitude_mesure)
        aKF.append(vec_altitude[0])
        e = np.linalg.norm(vec_reel[0] - vec_altitude[0])
        E += e * e
    plt.figure()
    plt.plot(T, tab, label='true altitude')
    plt.plot(T, mesure, label='measure')
    plt.plot(T, aKF, label='After Kalman Filter')
    plt.title("Réponse à une consigne d'altitude de 3m")
    plt.xlabel("Temps (s)")
    plt.ylabel("Altitude")
    plt.legend()
    plt.show()
    print("Fmax=" + str(Fmax))
    print("E=" + str(E))
    return E


##--_> 0.5x10e-5
def determinerQ():
    q0 = 0.00001 * np.eye(10)
    LN = []
    LN2 = []
    for i in range(1000):
        LN.append(np.random.normal(0, noisePositionLevel, 4))
        LN2.append(np.random.normal(0, noiseAngleLevel, 6))

    def f(qv):
        q = qv * np.eye(10)
        E = 0
        x = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        x_final = np.array([3, 0, 3, 0, 0, 0, 0, 0, 0, 0])
        cov = 0.0001 * np.identity(10)
        xk = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
        for i in range(1000):
            P = linalg.solve_discrete_are(A, B, Q, R, e=None, s=None, balanced=True)
            F_lqr = np.linalg.pinv(R + B.T @ P @ B) @ B.T @ P @ A  # F deja pris...
            u = -F_lqr @ (xk - x_final)
            x = (A @ x) + (B @ u)
            noisePosition = LN[i]
            noiseAngle = LN2[i]
            noise = np.concatenate([noisePosition, noiseAngle])
            z = x + noise
            xk, cov = kalmanFilterUpdate(A, B, xk, u, cov, z, q)
            e = np.linalg.norm(x - xk)
            E += e * e
            error = np.linalg.norm(xk - x_final)

            # Stop as soon as we reach the goal
            # Feel free to change this threshold value.
            if error < 0.1:
                break
        return E

    tests = np.geomspace(1, 40)
    results = []
    for t in tests:
        results.append(f(t * 1e-6 * np.eye(10)))
    plt.figure()
    plt.plot(tests * 1e-6, results)
    plt.xlabel("Valeur du process noise")
    plt.ylabel("Erreur quadratique du filtre de Kalman")
    plt.title("Minimisation de l'erreur quadratique du filtre de Kalman")
    plt.show()


def determinerQaltitude():
    tests = np.geomspace(4.5, 4.8)
    results = []
    for t in tests:
        results.append(testPID(t * 1e-6))
    plt.figure()
    plt.plot(tests * 1e-6, results)
    plt.xlabel("Valeur du process noise")
    plt.ylabel("Erreur quadratique du filtre de Kalman")
    plt.title("Minimisation de l'erreur quadratique du filtre de Kalman")
    plt.show()

