import numpy as np
from math import sqrt
import scipy.linalg as linalg
from simple_pid import PID
import matplotlib.pyplot as plt
import time

#### Variables
from Controle.code_brouillon import B_altitude, A_altitude

max_fin_angle = 45 # degree
noiseAltitudeLevel=0.01
noiseAltitudeVelocityLevel=0.01
noisePositionLevel=0.01
noisePositionVelocityLevel=0.01
noiseAngleLevel=0.001
noiseAngleVelocityLevel=0.001
M  = 4
g  = 9.81
Poids  = M*g
Fe = M*g
a  = 0.1*0.25
L  = 0.3
J  = 0.4
d  = 0.05
dt = 0.1
Q_Kf=2e-5*np.eye(10)
Q_altitude=np.eye(2)

B = dt*np.array([[0, 0, 0],
                [0, sqrt(3)*a/(2*M), -sqrt(3)*a/(2*M)],
                [0, 0, 0],
                [-a/M, a/(2*M), a/(2*M)],
                [0, 0, 0],
                [a*d/J, a*d/J, a*d/J],
                [0, 0, 0],
                [L/J, -L/(2*J), -L/(2*J)],
                [0, 0, 0],
                [0, sqrt(3)*L/(2*J), -sqrt(3)*L/(2*J)]])

R = 1000*np.identity(3) # Penalty for angular velocity effort
Q = 0.1*np.identity(10)
Q[4,4]=1000
Q[6,6]=1000
Q[8,8]=1000

def getA(F):
    dA = np.array([ [0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, F/M, 0],
                    [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, -F/M, 0, 0, 0],
                    [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])


    A = np.eye(10) + dt*dA
    return A

A=getA(Fe)
KalmanFilterOn=True
KalmanFilterAltitudeOn=False


####  Initialization

vec_altitude_estimate=np.array([0,0])
vec_altitude_measure=np.array([0,0])
x_estimate = np.array([0,0,0,0,0,0,0,0,0,0])
x_measure=np.array([0,0,0,0,0,0,0,0,0,0])
x_final = np.array([3,0,3,0,0,0,0,0,0,0])
u=np.array([0,0,0])
cov=0.0001*np.identity(10)
Pk=0.0001*np.identity(2)
pid = PID(0.07, 0.0001,1, setpoint=3) #setpoint is the objective, here an altitude of 3 meters
# sans dépassement :   pid = PID(0.07, 0.0001,1, setpoint=3)
# rapide :     pid = PID(0.2, 0.0001,1.25, setpoint=3)
pid.sample_time=0


#### Kalman Filter

def kalmanFilterUpdate(A, B, X, u, P, z, q):

    xf, Pf =kalmanFilterPredict(A, B, X, u, P, q)

    #Computation of the Observation Matrix
    H=np.eye(10)

    #Computation of Measurement Uncertainty
    R=np.eye(10)
    for i in range(0,4,2):
        R[i,i]=pow(noisePositionLevel,2)
    for i in range(1,4,2):
        R[i,i]=pow(noisePositionVelocityLevel,2)
    for i in range(4,10,2):
        R[i,i]=pow(noiseAngleLevel,2)
    for i in range(5,10,2):
        R[i,i]=pow(noiseAngleVelocityLevel,2)


    #Computation of the Kalman gain
    Ht=H.transpose()
    K=Pf@Ht@np.linalg.inv((H@Pf@Ht+R))

    #State update
    x = xf+K@(z-H@xf)

    #Covariance update
    IKH=(np.eye(10)-K@H)
    IKHt=IKH.transpose()
    Pnew=IKH@Pf@IKHt@+K@R@(K.transpose())

    return x,Pnew

def kalmanFilterPredict(A, B, X, u, P, q):
    xf=(A @ X) + (B @ u)

    Pf=A@P@(A.transpose())+q

    return xf, Pf

#Kalman Filter for altitude
def kalmanFilterAltitude(vecAltitude, Fe, P, vec_measure, q):

    vecAltitudef, Pf = kalmanFilterPredictAltitude(vecAltitude, Fe, P, q)

    #Computation of the Observation Matrix
    H=np.eye(2)

    #Computation of Measurement Uncertainty
    R=np.eye(2)
    R[0,0]=pow(noiseAltitudeLevel,2)
    R[1,1]=pow(noiseAltitudeVelocityLevel,2)


    #Computation of the Kalman gain
    Ht=H.transpose()
    K=Pf@Ht@np.linalg.inv((H@Pf@Ht+R))

    #State update
    vecAltitude = vecAltitudef+K@(vec_measure-H@vecAltitudef)

    #Covariance update
    IKH=(np.eye(2)-K@H)
    IKHt=IKH.transpose()
    Pnew=IKH@Pf@IKHt@+K@R@(K.transpose())+q

    return vecAltitude, Pnew

def kalmanFilterPredictAltitude(vecAltitude, Fe, P, q):
    vecAltitudef=A_altitude@vecAltitude + B_altitude*(Fe-Poids)/M
    P=A_altitude@P@(A_altitude.transpose())+q
    return vecAltitudef, P


#### Step

def step(xk, cov, vec_altitude_estimate, Pk, vec_altitude_measure, x_measure, x_final, u, Fe):

    A=getA(Fe)

    #Kalman Filter on measures
    if (KalmanFilterOn):
        xk, cov = kalmanFilterUpdate(A, B, xk, u, cov, x, Q_Kf)
    else:
        xk=x_measure
    if (KalmanFilterAltitudeOn):
        vec_altitude_estimate, Pk = kalmanFilterAltitude(vec_altitude_estimate, Fe, Pk, vec_altitude_measure, Q_altitude)
    else:
        vec_altitude_estimate=vec_altitude_measure

    #PID for altitude control


    Fe=pid(vec_altitude_estimate[0],dt)+Poids

    #LQR for horizontal and angular control
    A=getA(Fe)
    P = linalg.solve_discrete_are(A, B, Q, R, e=None, s=None, balanced=True)
    F_lqr = np.linalg.pinv(R+B.T@P@B) @ B.T @ P @ A  # F deja pris...
    u = -F_lqr @ (xk - x_final)

    return u, Fe, xk, cov, vec_altitude_estimate, Pk

### Simulation

x=np.array([0,0,0,0,0,0,0,0,0,0])
vecAltitude=np.array([0,0])
i=0
error = np.linalg.norm(x - x_final)
X=[x[0]]
T=[0]
while (i<1000):
    print(f't = {dt*i}s')
    T.append(T[-1]+dt)
    u, Fe, x_estimate, cov, vec_altitude_estimate, Pk = step(x_estimate, cov, vec_altitude_estimate, Pk, vec_altitude_measure, x_measure, x_final, u, Fe)

    vecAltitude=A_altitude@vecAltitude + B_altitude*(Fe-Poids)/M
    x=(A @ x) + (B @ u)
    X.append(x[0])

    noisePosition=np.random.normal(0,noisePositionLevel,4)
    noiseAngle=np.random.normal(0,noiseAngleLevel,6)
    noise = np.concatenate([noisePosition,noiseAngle])
    x_measure=x+noise
    vec_altitude_measure=vecAltitude

    error = np.linalg.norm(x - x_final)
    i+=1
    if error < 0.1:
        print("\nGoal Has Been Reached Successfully!")
        break
plt.figure()
plt.plot(T,X, label='true y')
plt.show()
