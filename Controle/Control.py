import math
import numpy as np
import scipy.linalg as linalg
from Controle.KalmanFilter import KalmanFilter


class Control:
    def __init__(self, dt, kalmanFilterOn):
        self.kalmanFilterOn = kalmanFilterOn
        self.dt = dt
        # Masse de la fusée.
        self.M = 2.117
        # Accélération de pesanteur.
        self.g = 9.81
        self.P = self.M * self.g
        # Coefficient de force pour les ailettes.
        self.a = - 0.0927 / self.g
        # Distance des ailettes au centre de masse selon l'axe x.
        self.L = 0.3
        # Inertie
        self.J = 0.2
        # Rayon de la fusée.
        self.r = 0.05

        # Bryson's Rule
        # Qi = 1/max(xi)²
        # Ri = 1/max(ui)²
        # phi .(roll.), theta,theta.,psi,psi.
        self.Q = np.array([[1000, 0, 0, 0, 0, 0],
                           [0, 1, 0, 0, 0, 0],
                           [0, 0, 1000, 0, 0, 0],
                           [0, 0, 0, 1, 0, 0],  # de l'ordre de 1° pour le tangage et le lacet
                           [0, 0, 0, 0, 1000, 0],
                           [0, 0, 0, 0, 0, 1]])
        self.R = np.array([[10, 0, 0],  # de l'ordre de 50° pour les ailettes
                           [0, 10, 0],
                           [0, 0, 10]])

        # Instancie le filtre de Kalman.
        self.kalmanFilter = KalmanFilter(dt, self.M, self.P)
        self.A = np.eye(6) + self.dt * np.array([[0, 1, 0, 0, 0, 0],
                                                 [0, 0, 0, 0, 0, 0],
                                                 [0, 0, 0, 1, 0, 0],
                                                 [0, 0, 0, 0, 0, 0],
                                                 [0, 0, 0, 0, 0, 1],
                                                 [0, 0, 0, 0, 0, 0]])

    # Matrice B dans l'équation >(t+dt) = A.W(t) + B.U
    def getB(self, Fe):
        return self.dt * np.array(
            [[0, 0, 0],
             [-self.a * Fe * self.r / self.J, -self.a * Fe * self.r / self.J, -self.a * Fe * self.r / self.J],
             [0, 0, 0],
             [0, -self.a * Fe * math.sqrt(3) * self.L / (2 * self.J),
              math.sqrt(3) * self.a * Fe * self.L / (2 * self.J)],
             [0, 0, 0],
             [-self.a * Fe * self.L / self.J, self.a * Fe * self.L / (2 * self.J),
              self.a * Fe * self.L / (2 * self.J)]])


    def step(self, estimate, cov, Fe, Pk, measure, U):
        # Utilisation du filtre de Kalman (s'il est activé) pour estimer l'état de la fusée.
        if self.kalmanFilterOn:
            estimate, cov = self.kalmanFilter.kalmanFilterUpdate(self.A, self.B, estimate, U, cov, measure, self.Q_Kf)
        else:
            estimate = measure
            B = self.getB(Fe)
            # Résolution des équations de Riccati discrètes pour trouver les angles d'ailettes optimaux.
            P = linalg.solve_discrete_are(self.A, B, self.Q, self.R, e=None, s=None, balanced=True)
            F_lqr = np.linalg.inv(self.R + B.T @ P @ B) @ B.T @ P @ self.A  # F deja pris...
            u = -F_lqr @ estimate

        return u, estimate, cov, Pk
