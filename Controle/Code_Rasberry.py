import time
import numpy as np
from Control import Control
from Controller import Controller
from IMUData import IMUData


def main():
    # Angles of the fins.
    U = [0, 0, 0]
    dt = 0.06
    control = Control(dt=dt)
    IMUdata = IMUData(dt)
    controller = Controller()

    x = np.array([0 for k in range(5)])
    estimate = x

    # covariance des mesures (pour le filtre de Kalman)
    cov = 0.0001 * np.identity(5)
    Pk = 0.0001 * np.identity(2)

    counter = 0
    # 5 secondes pour la 1ère phase ou le système est à l'arrêt.
    end_first_phase = 5
    # 10 secondes pour la 2ème phase ou le moteur passe progressivement à la poussée finale.
    end_second_phase = 16
    # 10 secondes de vol à la poussée nominale
    end_third_phase = 26

    # Masse ajoutée lors de la phase de montée.
    #La poussée du moteur est alors (M+rise_mass_delta)*g.
    rise_mass_delta = 0.5
    # Masse retirée lors de la phase de descente.
    fall_mass_delta = 2
    # Initialise les ailettes à 0 au début du vol.
    controller.set_servo_angle([0, 0, 0])

    # Sauvegarde les données dans un fichier csv.
    f = open("data/" + time.strftime("%d-%H-%M") + ".csv", "w")
    f.write("Number,dRoll,Pitch,dPitch,Yaw,dYaw,Force,Angle1,Angle2,Angle3,Time,Computation_Time\n")
    t_init = time.perf_counter()
    while True:
        t1 = time.perf_counter()
        if (t1 - t_init) < end_first_phase:
            time.sleep(dt)
            continue

        elif (t1 - t_init) < end_second_phase:
            Fe = ((control.M + rise_mass_delta) * control.g) * (
                (int)(counter * 5. / end_second_phase)) / 5.

        elif (t1 - t_init) < end_third_phase:
            Fe = ((control.M + rise_mass_delta) * control.g)

        else:
            Fe = ((control.M - fall_mass_delta) * control.g)
        #Recupère les données depuis l'IMU
        measure = IMUdata.getMeasure()
        #Applique la consigne de force au moteur.
        controller.motor_consign(Fe)
        t_init = time.perf_counter()
        # Calcul de la commande à appliquer sur les ailettes.
        U, estimate, cov, Pk = control.step(estimate, cov, Fe, Pk, measure, U)
        #Mise à jour de l'état du système.
        estimate = (control.A @ estimate) + (control.getB(Fe) @ U)

        # Applique la commande sur les ailettes.
        controller.set_servo_angle([0, 0, 0])
        t2 = time.perf_counter()

        # Print les données dans la console toutes les secondes.
        if counter % 20 == 0:
            print("Yaw: " + str(measure[3]))
            print("Yaw .: " + str(measure[4]))
            print("Pitch: " + str(measure[1]))
            print("Pitch .: " + str(measure[2]))
            print("-------------------")
            print()

        # Sauvegarde des données dans le fichier csv.
        f.write(str(counter) + "," + str(measure[0]) + "," + str(measure[1]) + "," + str(measure[2]) + "," + str(
            measure[3]) + "," + str(measure[4]) + "," + str(Fe) + "," + str(U[0]) + "," + str(U[1]) + "," + str(
            U[2]) + "," + str(time.perf_counter() - t_init) + "," + str(t2 - t1) + "\n")
        counter += 1

        # Attends le temps dt si l'algorithme a été plus rapide afin d'avoir une fréquence de calcul constante.
        if t2 - t1 < dt:
            time.sleep(dt - (t2 - t1))


if __name__ == "__main__":
    main()
