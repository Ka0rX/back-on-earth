import numpy as np
from vnpy import *

class IMUData:
    def __init__(self, dt):
        self.old_estimate = None
        self.dt = dt
        #Classe fournie par VectorNav pour récupérer les données de l'IMU.
        self.s = VnSensor()
        self.ypr_init = None
        self.s.connect('/dev/ttyUSB0', 115200)

        print(self.s.read_model_number())

    def to_rad(self, x):
        return x * np.pi / 180

    def from_vec3f(self, M):
        return np.array([M.x, M.y, M.z])

    # Lis les angles donnés par l'IMU et la convertit en rad.s^-1.
    # Opère un changement de base pour que les angles soient exprimés dans le repère de la fusée.
    def read_ypr(self):
        reg = self.s.read_yaw_pitch_roll_magnetic_acceleration_and_angular_rates()
        vec = self.to_rad(self.from_vec3f(reg.yaw_pitch_roll))
        vec = np.array([-vec[2], -vec[1], -vec[0]])
        return vec

    def getMeasure(self):
        ypr = np.array(self.read_ypr()).flatten()

        # Dérives les angles yaw, pitch et roll.
        if self.old_estimate is None:
            v_ypr = np.array([0, 0, 0])
        else:
            v_ypr = (ypr - self.old_estimate[0:3]) / self.dt

        # Ceci est utilisé pour calculer la dérivée pour le prochain pas de temps.
        self.old_estimate = np.array(
            [ypr[0], ypr[1], ypr[2]])

        # Retourne les angles roulis tanguage lacet et leurs dérivées.
        # Les angles correspondants sont respectivement : phi, theta et psi.
        return [ypr[2], v_ypr[2], ypr[1], v_ypr[1], ypr[0], v_ypr[0]]
