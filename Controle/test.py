import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

k = 2 * np.pi
w = 2 * np.pi
dt = 0.01

fig = plt.figure()
ax = plt.axes(projection='3d')

X = []
Y = []
Z = []

for i in range(50):
    t = i * dt
    plt.plot(X, Y, Z)
    plt.pause(1)

plt.show()
