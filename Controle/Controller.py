import pigpio
import math

"""
    Consigne pour etre a 0 pour chaque servo et pin de la raspberry associé.
    26: 1455 (max 2355 min 555)
    5 : 1540 (max 2440 min 640)
    6 : 1485 (max 2385 min 585) (26)
    Delta de +-900 a chaque fois (900-> 90 degré)
"""
class Controller:

    # A priori
    # 1 -> 19
    # 2 -> 12  (Peut etre inversé pour les 2?)
    # 3 -> 13
    def __init__(self):
        self.servos = [(26, 1455), (5, 1455), (6, 1485)]
        self.motor = (23, 1000)
        self.pi = pigpio.pi()

    # Convertit les angles en consignes pour les servos moteurs.
    # Restriction a +-30 degrés et prise en compte des signaux pour avoir 0° différent pour chaque servo.
    def angle_to_pulse(self, id, angle):
        angle = angle * (180 / math.pi)
        angle = min(max(angle, -30), 30)
        return self.servos[id][1] + 10 * int(angle * dir)


    def set_servo_angle(self, angles):
        for k in range(3):
            self.pi.set_servo_pulsewidth(self.servos[k][0], self.angle_to_pulse(k, angles[k]))

    # Convertit la force en consigne PMW(Pulse Width Modulation) pour le moteur grace
    # a la détermination de la loi du moteur et une approximation linéaire.
    # Une consigne de 1000 correspond à un moteur au repos et 2000 à la pleine puissance.
    def motor_consign(self, force):
        offset = max(min(1000 * (force / (3.6 * 9.81)), 1000), 0)
        self.pi.set_servo_pulsewidth(self.motor[0], self.motor[1] + offset)
