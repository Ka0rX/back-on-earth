import numpy as np

class KalmanFilter:
    def __init__(self, M, P):
        self.noiseAngleLevel = 0.001
        self.noiseAngleVelocityLevel = 0.001
        self.M = M
        self.P = P

    def kalmanFilterUpdate(self, A, B, X, u, P, x_measure, q):

        xf, Pf = self.kalmanFilterPredict(A, B, X, u, P, q)

        # Calcul de la Matrice d'Observation
        H = np.eye(6)

        # Calcul de l'incertitude de mesure
        R = np.eye(6)
        for i in range(0, 6, 2):
            R[i, i] = pow(self.noiseAngleLevel, 2)
        for i in range(1, 6, 2):
            R[i, i] = pow(self.noiseAngleVelocityLevel, 2)

        # Calcul de la matrice de gain de Kalman.
        Ht = H.transpose()
        K = Pf @ Ht @ np.linalg.inv((H @ Pf @ Ht + R))

        # Mise a jour de l'état.
        x = xf + K @ (x_measure - H @ xf)

        # Mise a jour de la matrice de covariance
        IKH = (np.eye(6) - K @ H)
        IKHt = IKH.transpose()
        Pnew = IKH @ Pf @ IKHt @ +K @ R @ (K.transpose())

        return x, Pnew

    def kalmanFilterPredict(self, A, B, X, u, P, q):
        xf = (A @ X) + (B @ u)

        Pf = A @ P @ (A.transpose()) + q

        return xf, Pf
