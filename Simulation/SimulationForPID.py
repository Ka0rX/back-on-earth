from Controle.Control import Control
import numpy as np
from simple_pid import PID
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def simuWithOnlyPID(U) :
    #Here U is a constant (no control) to see how the rocket behaves

    X=[0]
    Y=[0]
    Z=[0]
    T=[0]
    counter=0
    dt=0.05
    control=Control([5,0,0],dt,False,False)
    Fe = 0

    A_altitude=np.eye(2)
    A_altitude[0, 1] = dt
    B_altitude = np.array([0, dt])
    #vecAltitude is z, z.
    vecAltitude=np.array([0,0])
    x=np.array([0 for k in range(10)])
    while (counter<1000):

        A=control.getA(Fe)
        #For the simulation
        vec_altitude_estimate=vecAltitude
        # The PID controls the force (Fe-Mg) so that it is always close to 0.
        # This implies that the resulting force is P+result_pid
        Fe = control.pid(vec_altitude_estimate[0], dt) + control.P
        #For the z axis z=Az+b*Fe
        vecAltitude=A_altitude@vecAltitude + B_altitude*(Fe-control.P)/control.M
        x=(A @ x) + (control.B @ U)
        X.append(x[0])
        Y.append(x[1])
        Z.append(vecAltitude[0])
        T.append(T[-1]+dt)
        counter+=1
        if x[1]>0.1 or x[2]>0.1 :
            break

    ax = plt.axes()
    ax.plot(T,Z, 'gray')
    plt.show()

simuWithOnlyPID([0,0,0.0])
