import matplotlib.pyplot as plt
from math import *

import matplotlib.pyplot as plt
import numpy as np
from numpy.random import random

from Controle.Control import Control


def simulation():
    # Here U is a constant (no control) to see how the rocket behaves
    phi_der=[0]
    theta = [0]
    theta_der=[0]
    psi = [0]
    psi_der=[0]
    U1=[0]
    U2=[0]
    U3=[0]
    V=[0]
    T = [0]
    U = [0, 0, 0]
    dt = 0.05
    control = Control(dt)
    # At the beginning the real value and estimation is 0 (the reocket is still)
    x = np.array([0 for k in range(5)])
    estimate = x

    counter = 0
    # 5 second to go to full throttle
    end_first_phase_counter = 50
    # 10 seconds rising
    end_second_phase_counter = 150
    rise_mass_delta = 0.1
    fall_mass_delta = 0.1

    while counter < 1000:
        pos_noise = 2*pi/180
        measure = estimate + np.array([0, pos_noise * (random()-0.5), 0, pos_noise * (random()-0.5), 0])
        F=control.M*control.g
        U, estimate, cov, Pk = control.step(estimate, 0,F, 0, measure, U)
        estimate = (control.A @ estimate) + (control.getB(F) @ U)

        phi_der.append(180/pi*estimate[0])
        theta.append(180/pi*estimate[1])
        theta_der.append(180/pi*estimate[2])
        psi.append(180/pi*estimate[3])
        psi_der.append(180/pi*estimate[4])
        U1.append(180/pi*U[0])
        U2.append(180/pi*U[1])
        U3.append(180/pi*U[2])
        T.append(T[-1] + dt)
        #print("Servo 1:"+str(U[0]))
        #print("Servo 2:"+str(U[1]))
        #print("Servo 3:"+str(U[2]))
        counter += 1
    ax = plt.axes()
    plt.title("Simulation avec LQR")
    ax.set_xlabel("Time (s)")
    ax.set_ylabel("Angle (°)")
    #ax.plot(T, phi_der, label="Phi Der")
    ax.plot(T, theta, label="Tanguage")
    #ax.plot(T, theta_der, label="Theta Der")
    ax.plot(T, psi,label="Lacet")
    #ax.plot(T, psi_der, label="Psi Der")
    #ax.plot(T,U1,label="U1")
    #ax.plot(T,U2,label="U2")
    #ax.plot(T,U3,label="U3")
    plt.plot()
    ax.legend()
    plt.show()


simulation()

