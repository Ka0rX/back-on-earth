import numpy as np


class KalmanFilter:
    def __init__(self, dt, M, P):
        self.noiseAltitudeLevel = 0.01
        self.noiseAltitudeVelocityLevel = 0.01
        self.noisePositionLevel = 0.01
        self.noisePositionVelocityLevel = 0.01
        self.noiseAngleLevel = 0.001
        self.noiseAngleVelocityLevel = 0.001
        # TODO Understand these params
        self.A_altitude = np.eye(2)
        self.A_altitude[0, 1] = dt
        self.B_altitude = np.array([0, dt])
        self.M = M
        self.P = P

    def kalmanFilterUpdate(self, A, B, X, u, P, x_measure, q):

        xf, Pf = self.kalmanFilterPredict(A, B, X, u, P, q)

        # Computation of the Observation Matrix
        H = np.eye(10)

        # Computation of Measurement Uncertainty
        R = np.eye(10)
        for i in range(0, 4, 2):
            R[i, i] = pow(self.noisePositionLevel, 2)
        for i in range(1, 4, 2):
            R[i, i] = pow(self.noisePositionVelocityLevel, 2)
        for i in range(4, 10, 2):
            R[i, i] = pow(self.noiseAngleLevel, 2)
        for i in range(5, 10, 2):
            R[i, i] = pow(self.noiseAngleVelocityLevel, 2)

        # Computation of the Kalman gain
        Ht = H.transpose()
        K = Pf @ Ht @ np.linalg.inv((H @ Pf @ Ht + R))

        # State update
        x = xf + K @ (x_measure - H @ xf)

        # Covariance update
        IKH = (np.eye(10) - K @ H)
        IKHt = IKH.transpose()
        Pnew = IKH @ Pf @ IKHt @ +K @ R @ (K.transpose())

        return x, Pnew

    def kalmanFilterPredict(self, A, B, X, u, P, q):
        xf = (A @ X) + (B @ u)

        Pf = A @ P @ (A.transpose()) + q

        return xf, Pf

    # Kalman Filter for altitude
    def kalmanFilterAltitude(self, vecAltitude, Fe, P, vec_measure, q):

        vecAltitudef, Pf = self.kalmanFilterPredictAltitude(vecAltitude, Fe, P, q)

        # Computation of the Observation Matrix
        H = np.eye(2)

        # Computation of Measurement Uncertainty
        R = np.eye(2)
        R[0, 0] = pow(self.noiseAltitudeLevel, 2)
        R[1, 1] = pow(self.noiseAltitudeVelocityLevel, 2)

        # Computation of the Kalman gain
        Ht = H.transpose()
        K = Pf @ Ht @ np.linalg.inv((H @ Pf @ Ht + R))

        # State update
        vecAltitude = vecAltitudef + K @ (vec_measure - H @ vecAltitudef)

        # Covariance update
        IKH = (np.eye(2) - K @ H)
        IKHt = IKH.transpose()
        Pnew = IKH @ Pf @ IKHt @ +K @ R @ (K.transpose()) + q

        return vecAltitude, Pnew

    def kalmanFilterPredictAltitude(self, vecAltitude, Fe, P, q):
        vecAltitudef = self.A_altitude @ vecAltitude + self.B_altitude * (Fe - self.P) / self.M
        P = self.A_altitude @ P @ (self.A_altitude.transpose()) + q
        return vecAltitudef, P
