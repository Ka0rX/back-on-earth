from Controle_Ancien.Control import Control
import numpy as np
from simple_pid import PID
import matplotlib.pyplot as plt
import matplotlib.colors
from matplotlib.collections import LineCollection
from numpy.random import random

def simulation():
    # Here U is a constant (no control) to see how the rocket behaves

    X = [0]
    Y = [0]
    Z = [0]
    T = [0]
    U = [0, 0, 0]
    counter = 0
    dt = 0.05
    control = Control([2, 0, 0], dt, False, False)
    Fe = 0

    #vec_altitude : [z,z.]
    # vec_altitude =A*vec_altitude+B*(F-Mg)/M
    A_altitude = np.eye(2)
    A_altitude[0, 1] = dt
    B_altitude = np.array([0, dt])

    # vecAltitude is z, z.
    #At the beginning the real value and estimation is 0
    vecAltitude = np.array([0, 0])
    vec_altitude_estimate = vecAltitude

    #At the beginning the real value and estimation is 0 (the reocket is still)
    x = np.array([0 for k in range(10)])
    x_estimate=x

    #covariance of the measures (for the kalman filter)
    cov = 0.0001 * np.identity(10)

    #
    Pk = 0.0001 * np.identity(2)

    while (counter < 500):
        # For the simulation we introduce some noise in the measures. (5 cm for spatial and 5 degres: 0.025 for angles in the speed)
        pos_noise=0.1
        angle_noise=0.025
        vec_altitude_measure = vecAltitude + pos_noise*(random()-0.5)
        x_measure = x + np.array(
            [0, pos_noise*(random()-0.5), 0, pos_noise*(random()-0.5), 0, angle_noise*(random()-0.5), 0, angle_noise*(random()-0.5), 0, angle_noise*(random()-0.5)])

        U, Fe, x_estimate, cov, vec_altitude_estimate, Pk = control.step(x_estimate, cov, vec_altitude_estimate, Pk,
                                                                         vec_altitude_measure, x_measure, U, Fe)
        A = control.getA(Fe)

        # The PID controls the force (Fe-Mg) so that it is always close to 0.
        # This implies that the resulting force is P+result_pid
        Fe = control.pid(vec_altitude_estimate[0], dt) + control.P
        # For the z axis z=Az+b*Fe
        vecAltitude = A_altitude @ vecAltitude + B_altitude * (Fe - control.P) / control.M
        x = (A @ x) + (control.B @ U)
        X.append(x[0])
        Y.append(x[1])
        Z.append(vecAltitude[0])
        T.append(T[-1] + dt)
        counter += 1
        if vecAltitude[0]>2 :
            break
        #if x[1] > 0.1 or x[2] > 0.1:
        #    break

    ax = plt.axes()
    plt.title("Simulation avec les anciennes matrices")
    ax.set_xlabel("z (m)")
    ax.set_ylabel("y (m)")
    #ax.plot(T, phi_der, label="Phi Der")
    plt.scatter(X, Y, label="", c=T, cmap=plt.get_cmap('jet'))
    #ax.plot(T, theta_der, label="Theta Der")
    #ax.plot(T, psi,label="Lacet")
    #ax.plot(T, psi_der, label="Psi Der")
    #ax.plot(T,U1,label="U1")
    #ax.plot(T,U2,label="U2")
    #ax.plot(T,U3,label="U3")
    #plt.plot()
    cbar = plt.colorbar()
    cbar.ax.set_title("Temps (s)")
    ax.legend()
    plt.show()
    print(X)


    ax = plt.axes()
    plt.title("Simulation avec les anciennes matrices")
    ax.set_xlabel("z (m)")
    ax.set_ylabel("x (m)")
    #ax.plot(T, phi_der, label="Phi Der")
    plt.scatter(X, Z, label="", c=T, cmap=plt.get_cmap('jet'))
    #ax.plot(T, theta_der, label="Theta Der")
    #ax.plot(T, psi,label="Lacet")
    #ax.plot(T, psi_der, label="Psi Der")
    #ax.plot(T,U1,label="U1")
    #ax.plot(T,U2,label="U2")
    #ax.plot(T,U3,label="U3")
    #plt.plot()
    cbar = plt.colorbar()
    cbar.ax.set_title("Temps (s)")
    ax.legend()
    plt.show()

simulation()