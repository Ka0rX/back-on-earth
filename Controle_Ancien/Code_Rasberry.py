from Controle.Control import Control
import numpy as np
from simple_pid import PID
import matplotlib.pyplot as plt
import time
from numpy.random import random

from Controle.IMUData import IMUData


def main():
    # Here U is a constant (no control) to see how the rocket behaves
    sum_accel=np.array([0,0,0])
    X = [0]
    Y = [0]
    Z = [0]
    T = [0]
    U = [0, 0, 0]
    counter = 0
    dt = 0.05
    # No Kalman filter for now
    control = Control(finalPos=[5, 0, 0], dt=dt, kalmanFilterAltitudeOn=False, kalmanFilterOn=False)
    IMUdata = IMUData(dt)
    Fe = 0

    # vec_altitude : [z,z.]
    # vec_altitude =A*vec_altitude+B*(F-Mg)/M
    A_altitude = np.eye(2)
    A_altitude[0, 1] = dt
    B_altitude = np.array([0, dt])

    # vecAltitude is z, z.
    # At the beginning the real value and estimation is 0
    vecAltitude = np.array([0, 0])
    vec_altitude_estimate = vecAltitude

    # At the beginning the real value and estimation is 0 (the reocket is still)
    x = np.array([0 for k in range(10)])
    x_estimate = x

    # covariance of the measures (for the kalman filter)
    cov = 0.0001 * np.identity(10)

    #
    Pk = 0.0001 * np.identity(2)
    calculus_time = []
    counter = 0
    maxcount=100
    while counter < maxcount:
        t1 = time.perf_counter()
        x_measure,vec_altitude_measure = IMUdata.getXAltMeasure()
        U, Fe, x_estimate, cov, vec_altitude_estimate, Pk = control.step(x_estimate, cov, vec_altitude_estimate, Pk,
                                                                         vec_altitude_measure, x_measure, U, Fe)
        A = control.getA(Fe)

        # The PID controls the force (Fe-Mg) so that it is always close to 0.
        # This implies that the resulting force is P+result_pid
        Fe = control.pid(vec_altitude_estimate[0], dt) + control.P
        # For the z axis z=Az+b*Fe
        vecAltitude = A_altitude @ vecAltitude + B_altitude * (Fe - control.P) / control.M
        x = (A @ x) + (control.B @ U)
        accel= IMUdata.getAccel()


        X.append(x_estimate[0])
        Y.append(accel[1])
        Z.append(vecAltitude[0])
        T.append(T[-1] + dt)
        counter += 1
        t2 = time.perf_counter()
        calculus_time.append(t2 - t1)

    plt.plot(T,X)
    plt.show()


#TODO: On voit qu'il y aun problème sur les données qu'on a en sortie de l'IMU avec certainement un petit offset pour l'accéleration
# qui fait que en intégrant ca devient problématique

#Solution envisagée pour l'instant : On mesure le biais empirique et on le retire pour réajuster et recalibrer l'IMU.
#Problème : Ce biais dépend de l'orientation donc bof..

#Test si le biais est consistant : Non (5 mesures avec la fusée verticale, 100 point de mesure) le biais n'est jamais le meme pour chaque mesure

#In practice, this can be achieved by calibration and orthogonality measurement. (Article qui parle du biais d'un acceleromètre)
#Filtre de Kalman sinon


#C'est de manière systématique vers les x croissant et y croissant

main()
