from vnpy import *
import numpy as np
import math


class IMUData:
    def __init__(self, dt):
        self.old_estimate = None
        self.dt = dt

        self.s = VnSensor()

        self.s.connect('/dev/ttyUSB0', 115200)

        print(self.s.read_model_number())

    def rotate(self, yaw, pitch, roll):
        yawMatrix = np.matrix([
            [math.cos(yaw), -math.sin(yaw), 0],
            [math.sin(yaw), math.cos(yaw), 0],
            [0, 0, 1]
        ])

        pitchMatrix = np.matrix([
            [math.cos(pitch), 0, math.sin(pitch)],
            [0, 1, 0],
            [-math.sin(pitch), 0, math.cos(pitch)]
        ])

        rollMatrix = np.matrix([
            [1, 0, 0],
            [0, math.cos(roll), -math.sin(roll)],
            [0, math.sin(roll), math.cos(roll)]
        ])

        R = yawMatrix @pitchMatrix @ rollMatrix

        return R

    def to_rad(self, x):
        return x * np.pi / 180

    def from_vec3f(self, M):
        return np.array([M.x, M.y, M.z])

    # We read the acceleration and convert the vec3 representing it to anp array.
    def read_accel(self):
        reg = self.s.read_yaw_pitch_roll_magnetic_acceleration_and_angular_rates()
        return self.from_vec3f(reg.accel)

    # We read the acceleration and convert the vec3 representing it to anp array.
    def read_ypr(self):
        reg = self.s.read_yaw_pitch_roll_magnetic_acceleration_and_angular_rates()
        return self.to_rad(self.from_vec3f(reg.yaw_pitch_roll))

    def getAccel(self):
        acc = np.array(self.read_accel())
        ypr = np.array(self.read_ypr()).flatten()
        # We must rotate the acceleration from the rocket basis to the x y z basis and then retrieve the gravity.

        M = self.rotate(ypr[0], ypr[1], ypr[2])
        acc = M @ acc + np.array([0, 0, 9.807])
        return np.array(acc.tolist()[0])
    def getXAltMeasure(self):
        acc = self.getAccel()
        ypr = np.array(self.read_ypr()).flatten()
        # Integrates the acceleration


        # Flatten the acceleration

        if self.old_estimate is None:
            # Old speed in the columns 3,4,5 of the old_estimate vector
            v = self.dt * acc
            pos = self.dt * v
        else:
            # Old speed in the columns 3,4,5 of the old_estimate vector
            v = np.array(self.old_estimate[3:6]) + self.dt * acc
            pos = np.array(self.old_estimate[0:3]) + self.dt * v

        # Derivates the ypr

        # If it is the first measure we cant derivate the ypr.
        if self.old_estimate is None:
            v_ypr = np.array([0, 0, 0])
        else:
            v_ypr = (ypr - self.old_estimate[6:9]) / self.dt

        self.old_estimate = np.array(
            [pos[0], pos[1], pos[2], v[0], v[1], v[2], ypr[0], ypr[1], ypr[2], v_ypr[0], v_ypr[1], v_ypr[2]])

        #The angles given are yaw, pitch, roll (in the rocket axis : -> phi->roll=x , theta->y=pitch, psi->z=yaw)
        # In the basis where x' is vertical there is x' =z, y'=x, z'= y with x,y,z the classic base
        return [pos[0], v[0], pos[1], v[1], ypr[0], v_ypr[0], ypr[1], v_ypr[1], ypr[2], v_ypr[2]], [pos[2], v[2]]
