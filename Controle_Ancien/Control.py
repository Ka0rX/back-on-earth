import numpy as np
import math
import scipy.linalg as linalg
from simple_pid import PID

from Controle.KalmanFilter import KalmanFilter



class Control:
    #finalPos: xfinal,yfinal,zfinal
    def __init__(self,finalPos, dt, kalmanFilterOn, kalmanFilterAltitudeOn):
        self.kalmanFilterOn = kalmanFilterOn
        self.kalmanFilterAltitudeOn = kalmanFilterAltitudeOn
        self.dt = dt
        self.finalPos=finalPos
        #For the LQR
        self.x_final=np.array([self.finalPos[1],0,self.finalPos[2],0,0,0,0,0,0,0])
        self.M = 4
        self.g = 9.81
        self.P = self.M * self.g
        self.Fe = self.M * self.g
        #a -> coeficient de force pour lees ailettes
        self.a = 0.1 * 0.25
        self.L = 0.3
        self.J = 0.4
        #Rayon a priori
        self.d = 0.05

        self.Q = np.array([[100, 0, 0, 0, 0, 0, 0, 0, 0, 0],  # de l'ordre de 10cm pour y et z
                           [0, 100, 0, 0, 0, 0, 0, 0, 0, 0],  # l'ordre de 0.1ms-1 pour y', z'
                           [0, 0, 100, 0, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 100, 0, 0, 0, 0, 0, 0],
                           [0, 0, 0, 0, 10, 0, 0, 0, 0, 0],  # de l'ordre de 20° pour le roulis
                           [0, 0, 0, 0, 0, 10, 0, 0, 0, 0],
                           [0, 0, 0, 0, 0, 0, 1000, 0, 0, 0],  # de l'ordre de 1° pour le tangage et le lacet
                           [0, 0, 0, 0, 0, 0, 0, 1000, 0, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 1000, 0],
                           [0, 0, 0, 0, 0, 0, 0, 0, 0, 1000]])
        self.R = np.array([[10, 0, 0],  # de l'ordre de 20° pour les ailettes
                           [0, 10, 0],
                           [0, 0, 10]])

        self.Q_Kf = 2e-5 * np.eye(10)
        self.Q_altitude = np.eye(2)
        # x is the vertical axis so the PID controls the x axis.
        self.pid = PID(0.07, 0.0001, 1, setpoint=finalPos[0])  # setpoint is the objective, here an altitude of 3 meters

        #Instanciate the Kalman Filter Object
        self.kalmanFilter= KalmanFilter(dt,self.M)
        self.B = dt * np.array([[0, 0, 0],
                                [0, math.sqrt(3) * self.a / (2 * self.M), -math.sqrt(3) * self.a / (2 * self.M)],
                                [0, 0, 0],
                                [-self.a / self.M, self.a / (2 * self.M), self.a / (2 * self.M)],
                                [0, 0, 0],
                                [self.a * self.d / self.J, self.a * self.d / self.J, self.a * self.d / self.J],
                                [0, 0, 0],
                                [self.L / self.J, -self.L / (2 * self.J), -self.L / (2 * self.J)],
                                [0, 0, 0],
                                [0, math.sqrt(3) * self.L / (2 * self.J), -math.sqrt(3) * self.L / (2 * self.J)]])

    def getA(self, F):
        dA = np.array([[0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, F / self.M, 0],
                       [0, 0, 0, 1, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, -F / self.M, 0, 0, 0],
                       [0, 0, 0, 0, 0, 1, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 1, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
                       [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
        A = np.eye(10) + self.dt * dA
        return A

    #### Step
    # xk is the estimation

    #In the basis where x' is vertical there is x' =z, y'=x, z'= y with x,y,z the classic base
    def step(self, xk, cov, vec_altitude_estimate, Pk, vec_altitude_measure, x_measure, u, Fe):



        A = self.getA(Fe)

        # Kalman Filter on measures
        if (self.kalmanFilterOn):
            xk, cov = self.kalmanFilter.kalmanFilterUpdate(A, self.B, xk, u, cov, x_measure, self.Q_Kf)
        else:
            xk = x_measure

        if (self.kalmanFilterAltitudeOn):
            vec_altitude_estimate, Pk = self.kalmanFilter.kalmanFilterAltitude(vec_altitude_estimate, Fe, Pk, vec_altitude_measure,
                                                             self.Q_altitude)
        else:
            vec_altitude_estimate = vec_altitude_measure

        # PID for altitude control

        #The PID controls the force (Fe-Mg) so that it is always close to 0.
        #This implies that the resulting force is P+result_pid
        Fe = self.pid(vec_altitude_estimate[0], self.dt) + self.P

        # LQR for horizontal and angular control
        A = self.getA(Fe)
        P = linalg.solve_discrete_are(A, self.B, self.Q, self.R, e=None, s=None, balanced=True)
        F_lqr = np.linalg.pinv(self.R + self.B.T @ P @ self.B) @ self.B.T @ P @ A  # F deja pris...
        u = -F_lqr @ (xk - self.x_final)

        return u, Fe, xk, cov, vec_altitude_estimate, Pk
