import pigpio
class Controller:
    # 12: 1455 (max 2355 min 555)
    # 13 = 1540 (max 2440 min 640)
    # 19 = 1485 (max 2385 min 585)
    #Delta de +-900 a chaque fois (900-> 90 degré)

    #A priori
    #1 -> 19
    #2 -> 12  (Peut etre inversé pour les 2?)
    #3 -> 13
    def __init__(self):
        self.servos = [(19, 1455), (12, 1455), (13, 1485)]
        self.motor= (18,1000)
        self.pi = pigpio.pi()

    def angleToPulse(self,id, angle):
        ispos = (id < 2)
        dir = 2 * ispos - 1
 #2
        return self.servos[id][1] + int(40 * angle * dir)

    def set_servo_angle(self, angles):
        for k in range(3):
            self.pi.set_servo_pulsewidth(self.servos[k][0], self.angleToPulse(k, angles[k]))

     #La force max est de 4.3kg 1000-> 0 et 2000-> 100%
    def motor_consign(self,force):
        #Offset between 1000 and 0
        offset=max(min(1000*(force/4.3),1000),0)
        self.pi.set_servo_pulsewidth(self.motor[0],self.motor[1]+offset)